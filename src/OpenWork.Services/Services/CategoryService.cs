﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using OpenWork.DataAccess.Interfaces;
using OpenWork.Domain.Entities;
using OpenWork.Services.Dtos.Admins;
using OpenWork.Services.Interfaces;
using OpenWork.Services.Interfaces.Common;
using OpenWork.Services.ViewModels.Admins;
using OpenWork.Services.ViewModels.Workers;

namespace OpenWork.Services.Services
{
	public class CategoryService : ICategoryService
	{
		private readonly IUnitOfWork _repository;

		public CategoryService(IUnitOfWork repository)
		{
			_repository = repository;
		}

		public async Task<bool> CreateAsync(CategoryCreateDto dto)
		{
			Category category = new Category
			{
				
				Name = dto.Name
			};
			_repository.Categories.Add(category);
			return await _repository.SaveChangesAsync() > 0;
		}

		public async Task<bool> DeleteAsync(long id)
		{
			_ = await _repository.Categories.DeleteAsync(id);
			return await _repository.SaveChangesAsync() > 0;
		}

		public async Task<IEnumerable<CategoryViewModel>> GetAllAsync()
		{
			return (await _repository.Categories.GetAll().ToListAsync()).Select(x => new CategoryViewModel()
			{
				Id = x.Id,
				Name = x.Name,
				Skills = x.Skills.Select(y => new SkillViewModel()
				{
					CategoryId = y.CategoryId,
					Description = y.Description,
					Id = y.Id,
					Name = y.Name
				}).ToList()
			});
		}

		public async Task<CategoryViewModel> GetAsync(long id)
		{
			Category category = await _repository.Categories.GetAsync(id);
			return new CategoryViewModel()
			{
				Id = category.Id,
				Name = category.Name,
				Skills = category.Skills.Select(x => new SkillViewModel()
				{
					CategoryId = x.CategoryId,
					Description = x.Description,
					Id = x.Id,
					Name = x.Name

				}).ToList()
			};
		}

		public async Task<bool> UpdateAsync(long id, CategoryCreateDto dto)
		{
			Category entity = await _repository.Categories.GetAsync(id);
			entity.Name = dto.Name;
			_repository.Categories.Update(entity);
			return await _repository.SaveChangesAsync() > 0;
		}
	}
}
