﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenWork.DataAccess.Interfaces;
using OpenWork.Domain.Entities;
using OpenWork.Services.Common.Exceptions;
using OpenWork.Services.Common.Utils;
using OpenWork.Services.Dtos.Users;
using OpenWork.Services.Interfaces;
using OpenWork.Services.Interfaces.Common;
using OpenWork.Services.Interfaces.Security;
using OpenWork.Services.ViewModels.Users;

namespace OpenWork.Services.Services;

public class UserService : IUserService
{
	private readonly IUnitOfWork _repository;
	private readonly IIdentityService _identity;
	private readonly IHasher _hasher;
	private readonly IAuthManager _auth;
	private readonly IPaginatorService _paginator;
	private readonly IEmailService _email;
	private readonly int _pageSize = 20;


	public UserService(IUnitOfWork repository, IIdentityService identity, IHasher hasher, IAuthManager auth, IPaginatorService paginator, IEmailService email)
	{
		_repository = repository;
		_identity = identity;
		_hasher = hasher;
		_auth = auth;
		_paginator = paginator;
		_email = email;
	}

	public async Task<bool> DeleteAsync()
	{
		_ = await _repository.Users.DeleteAsync(_identity.Id);
		return await _repository.SaveChangesAsync() > 0;
	}

	public async Task<bool> ForgotPasswordAsync(string email)
	{
		User entity = await _repository.Users.GetAsync(email);
		if(entity is null)
			throw new StatusCodeException(System.Net.HttpStatusCode.NotFound, "User with this email not found");
		if(entity.Banned)
			throw new StatusCodeException(System.Net.HttpStatusCode.Forbidden, "You are banned");
		string newPassword = Guid.NewGuid().ToString();
		entity.Password = _hasher.Hash(newPassword, entity.Email);
		await _email.SendMailAsync(entity.Email, $"Your new password for Open Work is \"{newPassword}\" without quotes. You can change it simply by updating your account info after logging in with this password.");
		return await _repository.SaveChangesAsync() > 0;
	}

	public async Task<IEnumerable<UserBaseViewModel>> GetAllAsync(int page)
	{
		return (await _paginator.PaginateAsync(_repository.Users.GetAll(), new PaginationParams(_pageSize, page))).Select(
				x => new UserBaseViewModel
				{
					Surname = x.Surname,
					Admin = x.Admin,
					Id = x.Id,
					Name = x.Name,
				}
			);
	}

	public async Task<UserViewModel> GetAsync()
	{
		return await GetAsync(_identity.Id);
	}

	public async Task<UserViewModel> GetAsync(long id)
	{
		User entity = await _repository.Users.GetAsync(id);
		if(entity is null)
			throw new StatusCodeException(System.Net.HttpStatusCode.NotFound, "User not found");
		return new UserViewModel
		{
			Email = entity.Email,
			Id = entity.Id,
			Surname = entity.Surname,
			Admin = entity.Admin,
			Name = entity.Name,
		};
	}

	public async Task<UserBaseViewModel> GetBaseAsync(long id)
	{
		User entity = await _repository.Users.GetAsync(id);
		if(entity is null)
			throw new StatusCodeException(System.Net.HttpStatusCode.NotFound, "User not found");
		return new UserBaseViewModel
		{
			Surname = entity.Surname,
			Admin = entity.Admin,
			Id = entity.Id,
			Name = entity.Name,
		};
	}

	public async Task<UserBaseViewModel> GetBaseAsync()
	{
		return await GetBaseAsync(_identity.Id);
	}

	public async Task<string> LoginAsync(UserLoginDto dto)
	{
		User entity = await _repository.Users.GetAsync(dto.Email);
		if(entity is not null)
			if(entity.Banned)
				throw new StatusCodeException(System.Net.HttpStatusCode.Forbidden, "You are banned");
			else if(_hasher.Verify(entity.Password, dto.Password, entity.Email))
				return _auth.GenerateToken(entity);
			else
				throw new StatusCodeException(System.Net.HttpStatusCode.BadRequest, "Invalid password");
		else
			throw new StatusCodeException(System.Net.HttpStatusCode.NotFound, "User not found");
	}

	public async Task<bool> RegisterAsync(UserRegisterDto dto)
	{
		User result = await _repository.Users.GetAsync(dto.Email);
		if(result is not null)
			if(result.Banned)
				throw new StatusCodeException(System.Net.HttpStatusCode.Forbidden, "You are banned");
			else
				throw new StatusCodeException(System.Net.HttpStatusCode.BadRequest, "User with this email already exists");
		User entity = dto;
		entity.Password = _hasher.Hash(dto.Password, dto.Email);
		_ = _repository.Users.Add(entity);
		return await _repository.SaveChangesAsync() > 0;
	}

	public async Task<bool> UpdateAsync(UserRegisterDto dto)
	{
		User entity = await _repository.Users.GetAsync(_identity.Id);
		entity.Surname = dto.Surname;
		entity.EmailVerified = dto.Email == entity.Email;
		entity.Email = dto.Email;
		entity.Name = dto.Name;
		entity.Password = _hasher.Hash(dto.Password, dto.Email);
		_ = _repository.Users.Update(entity);
		return await _repository.SaveChangesAsync() > 0;
	}
}
