﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenWork.DataAccess.Interfaces;
using OpenWork.Domain.Entities;
using OpenWork.Services.Common.Exceptions;
using OpenWork.Services.Common.Utils;
using OpenWork.Services.Dtos.Workers;
using OpenWork.Services.Interfaces;
using OpenWork.Services.Interfaces.Common;
using OpenWork.Services.ViewModels.Workers.Additional;

namespace OpenWork.Services.Services;

public class BusynessService : IBusynessService
{
	private readonly IUnitOfWork _repository;
	private readonly IIdentityService _identity;
	private readonly IPaginatorService _paginator;
	private readonly int _pageSize = 20;

	public BusynessService(IUnitOfWork repository, IIdentityService identity, IPaginatorService paginator)
	{
		_repository = repository;
		_identity = identity;
		_paginator = paginator;
	}

	public async Task<bool> CreateAsync(BusynessCreateDto dto)
	{
		if(_repository.Busynesses.GetAll().Where(x => x.WorkerId == _identity.Id).Any(x => x.Start > dto.Start && x.Start < dto.End || x.End > dto.Start && x.End < dto.End))
			throw new StatusCodeException(System.Net.HttpStatusCode.BadRequest, "Already have busyness in this time");
		Busyness entity = new()
		{
			Start = dto.Start,
			End = dto.End,
			WorkerId = _identity.Id
		};
		_ = _repository.Busynesses.Add(entity);
		return await _repository.SaveChangesAsync() > 0;
	}

	public async Task<bool> DeleteAsync(long id)
	{
		_ = await _repository.Busynesses.DeleteAsync(id);
		return await _repository.SaveChangesAsync() > 0;
	}

	public async Task<IEnumerable<BusynessViewModel>> GetAllAsync(long workerId, int page)
	{
		return (await _paginator.PaginateAsync(_repository.Busynesses.GetAll().Where(x => x.WorkerId == workerId), new PaginationParams(_pageSize, page))).Select(x => (BusynessViewModel)x);
	}

	public async Task<IEnumerable<BusynessViewModel>> GetAllAsync(int page)
	{
		return (await _paginator.PaginateAsync(_repository.Busynesses.GetAll().Where(x => x.WorkerId == _identity.Id), new PaginationParams(_pageSize, page))).Select(x => (BusynessViewModel)x);
	}

	public async Task<IEnumerable<BusynessViewModel>> SearchAsync(BusynessSearchDto dto, int page)
	{
		return (await _paginator.PaginateAsync(_repository.Busynesses.GetAll()
			.Where(x => x.WorkerId == dto.WorkerId)
			.Where(x => DateOnly.FromDateTime(x.Start) > dto.FromDate && DateOnly.FromDateTime(x.End) < dto.FromDate && TimeOnly.FromDateTime(x.Start) > dto.FromTime && TimeOnly.FromDateTime(x.End) < dto.ToTime)
			, new PaginationParams(_pageSize, page))).Select(x => (BusynessViewModel)x);
	}
}
